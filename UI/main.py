#!/usr/bin/env python3

import serial
import serial.tools.list_ports
import datetime
import os
from pynput import keyboard

# This will hold the serial interface
Arduino = None

# These commands should be the same on the Arduino side
class SerialCommands:
    MOD0 = b'\x00'  # A byte for each pump module
    MOD1 = b'\x01'
    UP = b'\x02'    # the possible commands
    DOWN = b'\x03'
    ZERO = b'\x04'
    LEADINGBYTE = b'\x05'

def serialConnect():
    print("Checking for open serial ports...")
    arduino_ports = [
        p.device
        for p in serial.tools.list_ports.comports()
        if ('Arduino' in p.description) or ('ttyACM0' in p.description) or ('/dev/ttyUSB0' in p.description) # may need tweaking to match new arduinos
    ]
    all_ports = [
        p.device
        for p in serial.tools.list_ports.comports()
    ]

    if not arduino_ports:
        if not all_ports:
            print("No ports found, exiting")
            quit()
        print("No default port found, but we have these:")
        for i,port in enumerate(all_ports):
            print(str(i)+ ': ' + port)
        sel = input("please choose number...")
        motherboardport = all_ports[sel]
    else:
        motherboardport = arduino_ports[0]

    try:
        Arduino = serial.Serial(
                port = motherboardport,
                baudrate = 115200,
                parity = serial.PARITY_NONE,
                stopbits = serial.STOPBITS_ONE,
                bytesize = serial.EIGHTBITS,
                timeout = 1
                )
        Arduino.isOpen() # try to open port, if possible print message and proceed with 'while True:'
        print ("port is opened!")
        return Arduino

    except IOError: # if port is already opened, close it and open it again and print message
        Arduino.close()
        Arduino.open()
        print ("port was already open, was closed and opened again!")

# Collect keyboard input
def on_press(key):
    try:
        if key.char == 'p':
            quit()
        elif key.char == 'q':
            dev_byte = SerialCommands.MOD0 # device byte
            send_byte = SerialCommands.UP
        elif key.char == 'a':
            dev_byte = SerialCommands.MOD0
            send_byte =SerialCommands.ZERO
        elif key.char == 'z': 
            dev_byte = SerialCommands.MOD0 
            send_byte = SerialCommands.DOWN
        elif key.char == 'w':
            dev_byte = SerialCommands.MOD1
            send_byte = SerialCommands.UP
        elif key.char == 's':
            dev_byte = SerialCommands.MOD1 
            send_byte = SerialCommands.ZERO
        elif key.char == 'x':
            dev_byte = SerialCommands.MOD1 
            send_byte = SerialCommands.DOWN
        else:
            return
        Arduino.write(SerialCommands.LEADINGBYTE)
        Arduino.write(dev_byte)
        Arduino.write(send_byte)
    except AttributeError:
        pass

def clear_screen():
    try:
        _=os.system("cls")
        _=os.system("clear") 
    except:
        pass

def print_stats(commands,pressures):
    '''
    Inputs are both lists with an element for each pump module
    '''
    for i,(c,p) in enumerate(zip(commands,pressures)):
        print("Module{} | Command: {} | Actual: {}".format(i,c,p))


if __name__ == "__main__":
    Arduino = serialConnect()
    Arduino.reset_input_buffer()
    commands = [0,0]
    pressures = [0,0]

    # Collect keyboard events until released
    listener = keyboard.Listener(on_press=on_press)
    listener.start()
    while True:
        while Arduino.in_waiting:
            # This idiom has been used for a while, it works with arduino
            incoming = Arduino.readline().decode('unicode_escape').strip().split(':')
            # receive lines in the form [command|pressure][0|1]:val
            if 'command' in incoming[0]:
                commands[int(incoming[0][-1])] = incoming[1]
            elif 'pressure' in incoming[0]:
                pressures[int(incoming[0][-1])] = incoming[1]

            clear_screen()
            print_stats(commands,pressures)