#include <Arduino.h>
#include <constants.h>
#include <RavataPeristalticPump.h>

double actual_pressure[2];
double command_pressure[2];

RavataPeristalticPump pump0;
RavataPeristalticPump pump1;

unsigned long tasktime = millis();
const unsigned long tasklength = 100; // time between checking pressures

// read the pressures from the attinies and send that data to the pc
void readpressures()
{
  actual_pressure[0] = pump0.readPressure();
  actual_pressure[1] = pump1.readPressure();

  // send in the form [command|pressure][0|1]:val
  for (size_t i = 0; i < 2; i++)
  {
    Serial.print("command");
    Serial.print(i);
    Serial.print(":");
    Serial.println(command_pressure[i]);

    Serial.print("pressure");
    Serial.print(i);
    Serial.print(":");
    Serial.println(actual_pressure[i]);
  }
}

void checkSerial()
{
  while (Serial.available() >= 3)
  {
    uint8_t checkbyte = Serial.read();
    if (checkbyte == LEADINGBYTE)
    {
      uint8_t module = Serial.read();
      uint8_t command = Serial.read();

      switch (command)
      {
      case UP:
        command_pressure[module] += PRESSUREINCREMENT;
        break;
      case ZERO:
        command_pressure[module] = 0;
        break;
      case DOWN:
        command_pressure[module] -= PRESSUREINCREMENT;
        break;
      default:
        break;
      }

      // after changing the pressures, update them at the attiny
      pump0.setPressure(command_pressure[0]);
      pump1.setPressure(command_pressure[1]);
    }
    else
    {
      //flush serial
      while (Serial.available() > 0)
      {
        Serial.read();
      }
    }
  }
}

void setup()
{
  Serial.begin(115200);

  // upload each attiny code with the address in the following defines
  pump0.init(MOD0ADDR);
  pump1.init(MOD1ADDR);
}

void loop()
{
  checkSerial();

  if (millis() - tasktime > tasklength)
  {
    readpressures();
    tasktime = millis();
  }
}