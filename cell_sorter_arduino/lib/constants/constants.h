// Pin definitions

// Serial constants

enum SerialCommands : uint8_t
{
    MOD0 = 0,
    MOD1,
    UP,
    DOWN,
    ZERO,
    LEADINGBYTE,
};

// Control constants
#define PRESSUREINCREMENT 0.02

// Device addresses
#define MOD0ADDR 0x00
#define MOD1ADDR 0x01